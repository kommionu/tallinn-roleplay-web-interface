<div class="form-group row justify-content-center mt-5">
    <b>{{__('texts.five_m_server_user_and_character_data')}}:</b>
</div>
@php
    if(isset($whitelistData) and $whitelistData->extra_questions != "null" and $whitelistData->extra_questions != null) {
       $extraQuestions = json_decode($whitelistData->extra_questions);
    }
@endphp
<div class="form-group row">
    <div class="col-md-12">
        <input id="characterName" type="text"
               class="form-control"
               name="characterName"
               value="{{ isset($whitelistData) ? $whitelistData->characterName : old('characterName') }}" required
               placeholder="{{__('forms.character_name')}}"
        >
        <span class="small">{{__('texts.character_name_that_you_are_planing_to_use_in_game')}}</span>
        @if ($errors->has('characterName'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('characterName') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <input id="steamName" type="text"
               class="form-control"
               name="steamName" value="{{ isset($whitelistData) ? $whitelistData->steamName : old('steamName') }}"
               required placeholder="{{__('forms.steam_name')}}">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <input id="hex" type="text"
               class="form-control"
               name="hex" value="{{isset($whitelistData) ? substr($whitelistData->identifier, 6) :  old('hex') }}"
               required placeholder="{{__('forms.steam_hex')}}">
        <span class="small">{{__('texts.hex_can_be_found_by_visiting')}}
            <a target="_blank" href="http://vacbanned.com/">http://vacbanned.com/</a>
        </span>
        @if ($errors->has('hex'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('hex') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row justify-content-center mt-5">
    <h5>{{__('texts.extra_questions')}}:</h5>
</div>
<div class="form-group row justify-content-center">
    <p>
        <strong>
            {{__('texts.read_rules')}}<a href="{{route('help')}}" target="_blank">{{__('texts.rules_link')}}</a>
        </strong>
    </p>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="whenYouCanUseWeapons">{{__('forms.whenYouCanUseWeapons')}}</label>
    <textarea id="whenYouCanUseWeapons" type="text"
              class="form-control"
              name="whenYouCanUseWeapons" required
    >{{isset($extraQuestions->whenYouCanUseWeapons) ? $extraQuestions->whenYouCanUseWeapons :  old('whenYouCanUseWeapons') }}</textarea>
    @if ($errors->has('whenYouCanUseWeapons'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('whenYouCanUseWeapons') }}</strong>
        </p>
    @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="newLifeRule">{{__('forms.newLifeRule')}}</label>
    <textarea id="newLifeRule" type="text"
              class="form-control"
              name="newLifeRule" required
    >{{isset($extraQuestions->newLifeRule) ? $extraQuestions->newLifeRule :  old('newLifeRule') }}</textarea>
    @if ($errors->has('newLifeRule'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('newLifeRule') }}</strong>
        </p>
    @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="helmetMaskRules">{{__('forms.helmetMaskRules')}}</label>
    <textarea id="helmetMaskRules" type="text"
              class="form-control"
              name="helmetMaskRules" required
    >{{isset($extraQuestions->helmetMaskRules) ? $extraQuestions->helmetMaskRules :  old('helmetMaskRules') }}</textarea>
    @if ($errors->has('helmetMaskRules'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('helmetMaskRules') }}</strong>
        </p>
    @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="robbingOtherCitizen">{{__('forms.robbingOtherCitizen')}}</label>
    <textarea id="robbingOtherCitizen" type="text"
              class="form-control"
              name="robbingOtherCitizen" required
    >{{isset($extraQuestions->robbingOtherCitizen) ? $extraQuestions->robbingOtherCitizen :  old('robbingOtherCitizen') }}</textarea>
    @if ($errors->has('robbingOtherCitizen'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('robbingOtherCitizen') }}</strong>
        </p>
    @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="carTax">{{__('forms.carTax')}}</label>
    <textarea id="carTax" type="text"
              class="form-control"
              name="carTax" required
    >{{isset($extraQuestions->carTax) ? $extraQuestions->carTax :  old('carTax') }}</textarea>
    @if ($errors->has('carTax'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('carTax') }}</strong>
        </p>
    @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="vdmRdm">{{__('forms.vdmRdm')}}</label>
    <textarea id="vdmRdm" type="text"
              class="form-control"
              name="vdmRdm" required
    >{{isset($extraQuestions->vdmRdm) ? $extraQuestions->vdmRdm :  old('vdmRdm') }}</textarea>
    @if ($errors->has('vdmRdm'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('vdmRdm') }}</strong>
        </p>
    @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="oocIc">{{__('forms.oocIc')}}</label>
    <textarea id="oocIc" type="text"
              class="form-control"
              name="oocIc" required
    >{{isset($extraQuestions->oocIc) ? $extraQuestions->oocIc :  old('oocIc') }}</textarea>
    @if ($errors->has('oocIc'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('oocIc') }}</strong>
        </p>
    @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
    <label for="radioUsage">{{__('forms.radioUsage')}}</label>
    <textarea id="radioUsage" type="text"
              class="form-control"
              name="radioUsage" required
    >{{isset($extraQuestions->radioUsage) ? $extraQuestions->radioUsage :  old('radioUsage') }}</textarea>
    @if ($errors->has('radioUsage'))
        <p>
            <strong style="color: red; font-size: 14px">{{ $errors->first('radioUsage') }}</strong>
        </p>
    @endif
    </div>
</div>
