@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="container">
                @component('components.searchBar', ['routeName' => 'ambulanceSearchUser', 'placeholder' => __('forms.search_user')])
                @endcomponent
            </div>
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if(\App\Helpers\WantedHelper::isUserWanted($wanted))
                    <div class="alert alert-danger" role="alert">
                        {{__('messages.wanted_character')}}
                    </div>
                @endif

                <div class="row">
                	<div class="col-md-6">
                        @component('components.userDataBox', ['user' => $user, 'profilePicture' => $profilePicture])
                        @endcomponent
        			</div>
                </div>

                <div class="card mt-5">
                    <div class="card-header">
                        {{__('headers.actions')}}
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('emsRemoveTattoos')}}">
                            @csrf
                            <input type="hidden" name="patientId" value="{{$user->identifier}}">
                            <button type="submit" class="btn btn-warning" onclick="overlayOn()">
                                {{__('buttons.remove_tattoos')}}
                            </button>
                        </form>
                    </div>
                    @component('components.medicalHistoryTable', ['characters' => $characters, 'identifier' => $user->identifier, 'medicalHistory' => $medicalHistory, 'actions' => true])
                        @slot('createMedicalIssueButton')
                            <form method="POST" action="{{route('createMedicalHistory')}}">
                                @csrf
                                <input type="hidden" name="patientIdentifier" value="{{$user->identifier}}">
                                <button type="submit" class="btn btn-info mb-3" onclick="overlayOn()">
                                    {{__('buttons.create_new_record')}}
                                </button>
                            </form>
                        @endslot
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
