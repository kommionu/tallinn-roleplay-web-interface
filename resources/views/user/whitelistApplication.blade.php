@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header justify-content-center">Whitelist Application With Web user registration</div>
                    <div class="card-body">
                        <form method="POST" action="">
                            @csrf
                        @component('components.whitelistApplication', ['whitelistData' => $whitelistData])
                        @endcomponent
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" onclick="overlayOn()">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
