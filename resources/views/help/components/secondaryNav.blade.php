<ul class="nav nav-tabs nav-tabs-dark">
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'help') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('help')}}" onclick="overlayOn()">{{__('link.server_rules')}}</a>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'info') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('info')}}" onclick="overlayOn()">{{__('link.server_info')}}</a>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'policeRules') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('policeRules')}}" onclick="overlayOn()">{{__('link.police_rules')}}</a>
    </li>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'emsRules') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('emsRules')}}" onclick="overlayOn()">{{__('link.ems_rules')}}</a>
    </li>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'showFinesInRules') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('showFinesInRules')}}" onclick="overlayOn()">{{__('link.fines_list')}}</a>
    </li>
</ul>
