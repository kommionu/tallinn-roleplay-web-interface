@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">{{__('tables.name')}}</th>
                            <th scope="col">{{__('tables.model')}}</th>
                            <th scope="col">{{__('tables.price')}}</th>
                            <th scope="col">{{__('tables.category')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($vehicles as $vehicle)
                        <tr>
                            <th scope="row">{{ $vehicle->id }}</th>
                            <td>{{ $vehicle->name }}</td>
                            <td>{{ $vehicle->model }}</td>
                            <td>{{ $vehicle->price }}</td>
                            <td>{{ $vehicle->category }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
