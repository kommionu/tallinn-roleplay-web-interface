<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parool peab olema vähemalt 6 tähemärki pikk ja kattuma kinnitus parooliga.',
    'reset' => 'Sinu parool on lähtestatud!',
    'sent' => 'Saatsime sulle e-mailile parool lähtesetamise lingi!',
    'token' => 'See parooli lähtestamise token on vale.',
    'user' => "Me ei leia sellise e-mailiga kasutajat.",

];
