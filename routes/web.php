<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('register/user', 'Auth\UserController@create')->name('userRegister');
Route::post('register/user','Auth\UserController@store')->name('userCreate');
Route::get('whitelist/application', 'Auth\WhitelistController@create')->name('whitelistApplication');
Route::post('whitelist/application', 'Auth\WhitelistController@store')->name('whitelistApplicationStore');

Route::get('help', 'HelpController@rules')->name('help');
Route::get('info', 'HelpController@info')->name('info');
Route::get('policeRules', 'HelpController@policeRules')->name('policeRules');
Route::get('emsRules', 'HelpController@emsRules')->name('emsRules');
Route::get('fines', 'HelpController@showFines')->name('showFinesInRules');
Route::get('search/fines', 'HelpController@searchFines')->name('searchFinesInRules');

Route::get('userAutocompleteSearch', 'Controller@userSearchAutocomplete')->name('userSearchAutocomplete');
Route::get('carPlateSearchAutocomplete', 'Controller@carPlateSearchAutocomplete')->name('carPlateSearchAutocomplete');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/cars', 'HomeController@cars')->name('myCars');
Route::get('/home/criminalRecords', 'HomeController@criminalRecords')->name('myCriminalRecords');
Route::get('/home/medicalRecords', 'HomeController@medicalRecords')->name('myMedicalRecords');
Route::post('/sellCar', 'HomeController@sellCar')->name('sellCar');
Route::post('/cancelCarSell', 'HomeController@cancelCarSell')->name('cancelCarSell');
Route::get('/applicationStatus', 'HomeController@whitelistApplicationStatus')->name('whitelistApplicationStatus');
Route::get('/application/update', 'HomeController@whitelistApplicationUpdateView')->name('whitelistApplicationUpdateView');
Route::post('/application/update', 'HomeController@updateWhitelistApplication')->name('updateWhitelistApplication');
Route::get('/applications/job/status', 'HomeController@showJobApplications')->name('showJobApplications');
Route::post('/unDeclareCarAsStolen', 'HomeController@unDeclareCarAsStolen')->name('homeUnDeclareCarAsStolen');
Route::post('/uploadProfilePicture', 'HomeController@uploadProfilePicture')->name('homeUploadProfilePicture');

Route::get('jobApplication/police', 'JobApplicationController@policeApplicationIndex')->name('jobApplicationPolice');
Route::get('jobApplication/ambulance', 'JobApplicationController@ambulanceApplicationIndex')->name('jobApplicationAmbulance');
Route::post('jobApplication/store', 'JobApplicationController@storeApplication')->name('jobApplicationStore');

Route::get('/admin/users-list', 'AdminController@allUsers')->name('adminAllUsers');
Route::get('/admin/user/{steamId}', 'AdminController@singleUser')->name('adminSingleUser');
Route::get('/admin/search/user', 'AdminController@searchUser')->name('adminSearchUser');
Route::post('/admin/characterKill/user', 'AdminController@characterKill')->name('adminCharacterKill');
Route::post('/admin/whitelistBan/banUser', 'AdminController@banFromWhitelist')->name('adminBanFromWhitelist');
Route::get('/admin/whitelistApplications', 'AdminController@showWhitelistApplications')->name('adminAllWhitelistApplications');
Route::post('/admin/whitelistApplications/reject', 'AdminController@rejectWhitelistApplication')->name('adminRejectWhitelistApplication');
Route::post('/admin/whitelistApplications/accept', 'AdminController@acceptWhitelistApplication')->name('adminAcceptWhitelistApplication');
Route::get('/admin/ckCandidates', 'AdminController@ckCandidates')->name('adminCkCandidates');

Route::get('/police/users-list', 'PoliceController@allUsers')->name('policeAllUsers');
Route::get('/police/search/user', 'PoliceController@searchUser')->name('policeSearchUser');
Route::get('/police/user/{steamId}', 'PoliceController@singleUser')->name('policeSingleUser');
Route::get('/police/user/cars/{steamId}', 'PoliceController@singleUserCars')->name('policeSingleUserCars');
Route::post('/police/delete/car', 'PoliceController@deleteCar')->name('policeDeleteCar');
Route::post('/police/delete/bill', 'PoliceController@deleteFine')->name('policeDeleteBill');
Route::post('/police/criminalRecord/create', 'PoliceController@createCriminalRecord')->name('createCriminalRecord');
Route::get('/police/criminalRecord/edit/{id}', 'PoliceController@showEditCriminalRecord')->name('showEditCriminalRecord');
Route::post('/police/criminalRecord/update', 'PoliceController@updateCriminalRecord')->name('updateCriminalRecord');
Route::post('/police/criminalRecord/add', 'PoliceController@addCriminalRecord')->name('addCriminalRecord');
Route::post('/police/criminalRecord/delete', 'PoliceController@deleteCriminalRecord')->name('policeDeleteReport');
Route::get('/police/criminalRecord/show/{id}', 'PoliceController@showCriminalRecord')->name('policeShowReport');
Route::get('/police/criminalRecord/showAll/', 'PoliceController@showAllReports')->name('policeShowAllReports');
Route::get('/police/criminalRecord/search/', 'PoliceController@searchReports')->name('policeSearchReports');
Route::post('/police/user/declareAsFugitive', 'PoliceController@declareAsFugitive')->name('policeDeclareAsFugitive');
Route::post('/police/user/unDeclareAsFugitive', 'PoliceController@unDeclareAsFugitive')->name('policeUnDeclareAsFugitive');
Route::get('/police/cars/all', 'PoliceController@showAllCars')->name('policeShowAllCars');
Route::get('/police/cars/search', 'PoliceController@searchCar')->name('searchCar');
Route::get('/police/fines', 'PoliceController@showFines')->name('showFines');
Route::get('/police/fines/search', 'PoliceController@searchFines')->name('searchFines');
Route::get('/police/fines/addNew', 'PoliceController@showAddNewFine')->name('showAddNewFine');
Route::post('/police/fines/create', 'PoliceController@createNewFine')->name('createNewFine');
Route::get('/police/fines/edit/{id}', 'PoliceController@showEditFine')->name('showEditFine');
Route::post('/police/fines/update', 'PoliceController@updateFine')->name('updateFine');
Route::post('/police/fines/remove', 'PoliceController@removeFine')->name('removeFine');
Route::get('/police/officers', 'PoliceController@showPoliceOfficersList')->name('showPoliceOfficersList');
Route::get('/police/applications', 'PoliceController@showApplicationsList')->name('policeShowApplicationsList');
Route::get('/police/application/{applicationId}', 'PoliceController@showSingleApplication')->name('policeShowSingleApplication');
Route::get('/police/applications/accepted', 'PoliceController@showAcceptedJobApplications')->name('policeShowAcceptedJobApplications');
Route::get('/police/application/accepted/{applicationId}', 'PoliceController@showSingleAcceptedApplication')->name('policeShowSingleAcceptedApplication');
Route::post('/police/application/accept', 'PoliceController@acceptApplication')->name('policeAcceptApplication');
Route::post('/police/application/reject', 'PoliceController@rejectApplication')->name('policeRejectApplication');
Route::post('/police/application/initiate', 'PoliceController@initiateApplicantToWork')->name('policeInitiateApplicantToWork');
Route::get('/police/car/{plate}', 'PoliceController@showSingleCar')->name('policeShowSingleCar');
Route::post('/police/declareWanted/car', 'PoliceController@declareCarAsWanted')->name('policeDeclareCarAsWanted');
Route::post('/police/declareStolen/car', 'PoliceController@declareCarAsStolen')->name('policeDeclareCarAsStolen');
Route::post('/police/unDeclareWanted/car', 'PoliceController@unDeclareCarAsWanted')->name('policeUnDeclareCarAsWanted');
Route::post('/police/unDeclareStole/car', 'PoliceController@UnDeclareCarAsStolen')->name('policeUnDeclareCarAsStolen');
Route::get('/police/allBills', 'PoliceController@allBills')->name('showAllPoliceBill');
Route::post('/police/setCallSign', 'PoliceController@setCallSign')->name('setCallSign');

Route::get('carDealer/usedCars/sellRequestList', 'CarDealerController@showUsedCarSellRequest')->name('usedCarsSellRequests');
Route::get('carDealer/usedCars/sellRequestHistory', 'CarDealerController@showUsedCarSellHistory')->name('usedCarsSellHistory');
Route::get('carDealer/usedCars/sellRequestSearch', 'CarDealerController@searchCarSellRequest')->name('usedCarsSellRequestSearch');
Route::get('carDealer/usedCars/sellRequestHistorySearch', 'CarDealerController@searchCarSellHistory')->name('sellRequestHistorySearch');
Route::get('carDealer/usedCars/sellRequest/{sellRequestId}', 'CarDealerController@showCarSellRequest')->name('usedCarsSellRequest');
Route::post('carDealer/usedCars/finalizeUsedCarSale', 'CarDealerController@finalizeUsedCarSale')->name('finalizeUsedCarSale');
Route::get('carDealer/vehiclesList', 'CarDealerController@showAllVehicles')->name('showAllVehicles');
Route::get('carDealer/allCars', 'CarDealerController@showAllCars')->name('showAllCars');
Route::get('carDealer/cars/{plate}', 'CarDealerController@showSingleCar')->name('carDealerShowSingleCar');
Route::post('carDealer/cars/model/update', 'CarDealerController@updateCarModelName')->name('carDealerUpdateCarModelName');
Route::post('carDealer/cars/insurance/update', 'CarDealerController@updateInsurance')->name('carDealerUpdateInsurance');
Route::get('carDealer/search/car', 'CarDealerController@searchCar')->name('carDealerSearchCar');

Route::get('/ambulance/users-list', 'AmbulanceController@allUsers')->name('ambulanceAllUsers');
Route::get('/ambulance/search/user', 'AmbulanceController@searchUser')->name('ambulanceSearchUser');
Route::get('/ambulance/user/{steamId}', 'AmbulanceController@singleUser')->name('ambulanceSingleUser');
Route::post('/ambulance/medicalIncident/create', 'AmbulanceController@createMedicalIncident')->name('createMedicalHistory');
Route::post('/ambulance/medicalIncident/add', 'AmbulanceController@addMedicalIncident')->name('addMedicalIncident');
Route::post('/ambulance/medicalIncident/delete', 'AmbulanceController@deleteMedicalIncident')->name('deleteMedicalIncident');
Route::get('/ambulance/medicalIncident/show/{id}', 'AmbulanceController@showMedicalIncident')->name('showMedicalIncident');
Route::get('/ambulance/applications', 'AmbulanceController@showApplicationsList')->name('emsShowApplicationsList');
Route::get('/ambulance/application/{applicationId}', 'AmbulanceController@showSingleApplication')->name('emsShowSingleApplication');
Route::post('/ambulance/application/accept', 'AmbulanceController@acceptApplication')->name('emsAcceptApplication');
Route::post('/ambulance/application/reject', 'AmbulanceController@rejectApplication')->name('emsRejectApplication');
Route::get('/ambulance/applications/accepted', 'AmbulanceController@showAcceptedJobApplications')->name('emsShowAcceptedJobApplications');
Route::get('/ambulance/application/accepted/{applicationId}', 'AmbulanceController@showSingleAcceptedApplication')->name('emsShowSingleAcceptedApplication');
Route::post('/ambulance/application/initiate', 'AmbulanceController@initiateApplicantToWork')->name('emsInitiateApplicantToWork');
Route::get('/ambulance/workers', 'AmbulanceController@showEmsWorkersList')->name('showEmsWorkersList');
Route::post('/ambulance/removeTattoo', 'AmbulanceController@removeTattoos')->name('emsRemoveTattoos');
Route::get('/ambulance/allBills', 'AmbulanceController@allBills')->name('showAllEmsBill');

Route::get('/mechanic/newReport', 'MechanicController@newReport')->name('mechanicNewReport');
Route::post('/mechanic/newReport', 'MechanicController@saveNewReport')->name('mechanicSaveNewReport');
Route::get('/mechanic/myReports', 'MechanicController@myReports')->name('mechanicMyReports');
Route::get('/mechanic/allReports', 'MechanicController@allReports')->name('mechanicAllReports');
Route::get('/mechanic/allBills', 'MechanicController@allBills')->name('showAllMechanicBill');
