<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsTo;

/**
 * Class JobApplication
 */
class JobApplication extends Model
{

    /**
     * @return BelongsTo
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function webUser()
    {

        return $this->belongsTo(WebUser::class);
    }
}
