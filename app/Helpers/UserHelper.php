<?php
namespace App\Helpers;

class UserHelper
{

    /**
     * @param mixed $result
     * @return string
     */
    public static function getCharacterName($result)
    {

        return $result->firstname . ' ' . $result->lastname;
    }
}
