<?php
namespace App\Helpers\Sql;

use App\CriminalRecord;
use Illuminate\Support\Facades\DB;

/**
 * Class FivemUserHelper
 */
class FivemUserHelper
{

    /**
     * search fivem user from users table
     *
     * @param string $keyword
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     * @throws \Exception
     */
    public static function searchUsers($keyword)
    {

        try {
            $keyword = strtolower($keyword);
            if(strpos($keyword, 'tel:') !== false) {
                $keyword = explode(':',$keyword)[1];
                $keyword = '%' . $keyword. '%';
                return DB::table('users')
                    ->where('phone_number', 'like', $keyword)
                    ->get();
            }

            if(strpos($keyword, 'steam:') !== false) {
                $keyword = '%' . $keyword. '%';
                return DB::table('users')
                    ->where('identifier', 'like', $keyword)
                    ->get();
            }

            if(count(explode(' ',$keyword)) > 1) {
                $keywords = explode(' ',$keyword);
                return DB::table('users')
                    ->whereRaw('LOWER(`firstname`) like :firstname and LOWER(`lastname`) like :lastname', ['firstname' => '%' . $keywords[0] . '%', 'lastname' => '%' . $keywords[1] . '%'])
                    ->get();
            }

            $keyword = '%' . $keyword. '%';
            return DB::table('users')
                ->whereRaw('LOWER(`name`) like ?', $keyword)
                ->orWhereRaw('LOWER(`firstname`) like ?', $keyword)
                ->orWhereRaw('LOWER(`lastname`) like ?', $keyword)
                ->get();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * search fivem user from users table
     *
     * @param string $keyword
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     * @throws \Exception
     */
    public static function searchRecords($keyword)
    {

        try {

            $keyword = '%' . strtolower($keyword) . '%';

            return CriminalRecord::with(['user', 'policeOfficer'])
                ->whereRaw('`id` like ?', $keyword)
                ->orWhereRaw('LOWER(`character_name`) like ?', $keyword)
                ->orWhereRaw('LOWER(`police_officer_id`) like ?', $keyword)
                ->get();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param $steamId
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public static function getUserCars($steamId)
    {

        try {
            return DB::table('owned_vehicles')
                ->where('owner', '=', $steamId)
                ->get();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param $carPlate
     * @return int
     * @throws \Exception
     */
    public static function deleteUserCar($carPlate)
    {

        try {
            return DB::table('owned_vehicles')
                ->where('plate', '=', $carPlate)
                ->delete();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
}
