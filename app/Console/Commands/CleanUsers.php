<?php

namespace App\Console\Commands;

use App\Helpers\Sql\CharacterKillHelper;
use App\Helpers\Sql\FivemUserHelper;
use App\User;
use App\Whitelist;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CleanUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all users and their data, who is not whitelisted';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Stared cleaning users and their data, who is not whitelisted');
        $this->info('Generating list of whitelisted steam hexes:');

        $whitelistCollection = Whitelist::all();
        $whitelistedHexes = [];
        $whitelistBar = $this->output->createProgressBar(count($whitelistCollection));
        $whitelistBar->start();
        foreach ($whitelistCollection as $whitelist) {
            $whitelistedHexes[] = $whitelist->identifier;
            $whitelistBar->advance();
        }
        $whitelistBar->finish();

        $this->info('');
        $this->info('Removing users:');
        $users = User::all();

        $userBar = $this->output->createProgressBar(count($users));
        $userBar->start();
        $data = [
            'coveredUsers' => 0,
            'deletedUsers' => 0,
            'untouchedUsers' => 0,
            'errors' => 0
        ];
        foreach ($users as $user) {
            $data['coveredUsers']++;
            if (!in_array($user->identifier, $whitelistedHexes)) {
                try {
                    CharacterKillHelper::removeAddOnAccountData($user->identifier);
                    CharacterKillHelper::removeAddOnInventoryItems($user->identifier);
                    CharacterKillHelper::removeBills($user->identifier);
                    CharacterKillHelper::removeCreatedBills($user->identifier);
                    CharacterKillHelper::removeCarSellRequests($user->identifier);
                    CharacterKillHelper::removeCharacters($user->identifier);
                    CharacterKillHelper::removeCriminalRecords($user->identifier);
                    CharacterKillHelper::removeDataStoreData($user->identifier);
                    CharacterKillHelper::removeMedicalHistory($user->identifier);
                    CharacterKillHelper::removeOwnedDock($user->identifier);
                    CharacterKillHelper::removeOwnedProperties($user->identifier);
                    CharacterKillHelper::removeOwnedVehicles($user->identifier);
                    CharacterKillHelper::removePhoneCalls($user->phone_number);
                    CharacterKillHelper::removePhoneMessages($user->phone_number);
                    CharacterKillHelper::removePhoneUserContacts($user->identifier, $user->phone_number);
                    CharacterKillHelper::removePlayersTattoos($user->identifier);
                    CharacterKillHelper::removeUserAccounts($user->identifier);
                    CharacterKillHelper::removeUserInventory($user->identifier);
                    CharacterKillHelper::removeUserLicenses($user->identifier);
                    CharacterKillHelper::removeWantedCharacters($user->identifier);
                    CharacterKillHelper::removeCreatedCriminalRecords($user->identifier);
                    CharacterKillHelper::removeCreatedMedicalRecords($user->identifier);
                    CharacterKillHelper::removeUsers($user->identifier);

                    $data['deletedUsers']++;
                }catch (\Exception $exception) {
                    $data['errors']++;
                    Log::error('User cleanup error for ' . $user->identifier . ': ' . $exception->getMessage());
                }
            } else {
                $data['untouchedUsers']++;
            }
            $userBar->advance();
        }
        $userBar->finish();

        $this->info('');
        $this->info('covered users: ' . $data['coveredUsers']);
        $this->info('Deleted users: ' . $data['deletedUsers']);
        $this->info('Untouched users: ' . $data['untouchedUsers']);
    }
}
