<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_accounts';
}
