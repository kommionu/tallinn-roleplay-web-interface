<?php
namespace App\Http\Controllers;

use App\FineType;

use App\Helpers\Sql\FivemFineTypes;

use Illuminate\Http\Request;

use Illuminate\View\View as View;

/**
* Help controller for rules and info pages
*/
class HelpController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {}

    /**
     * Show rules page
     *
     * @return View
     * @throws \Exception
     */
    public function rules()
    {

        return view('help.rules');
    }/**
     * Show rules page
     *
     * @return View
     * @throws \Exception
     */
    public function info()
    {

        return view('help.info');
    }

    /**
     * Show rules page
     *
     * @return View
     * @throws \Exception
     */
    public function policeRules()
    {

        return view('help.policeRules');
    }
    /**
     * Show rules page
     *
     * @return View
     * @throws \Exception
     */
    public function emsRules()
    {

        return view('help.emsRules');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showFines()
    {

        return view(
            'police.fines',
            [
                'fineSearchRoute' => 'searchFinesInRules',
                'fines' => FineType::paginate(20)
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchFines(Request $request)
    {

        return view(
            'police.fines',
            [
                'fineSearchRoute' => 'searchFinesInRules',
                'fines' => FivemFineTypes::searchFines($request->search),
                'oldSearch' => $request->search
            ]
        );
    }
}
