<?php

namespace App\Http\Controllers;

use App\AddonAccountData;
use App\Bill;
use App\CarSellRequest;
use App\Character;
use App\Helpers\Sql\FivemUserHelper;
use App\Helpers\Sql\WebUserHelper;
use App\Job;
use App\Motel;
use App\OwnedCar;
use App\OwnedMotel;
use App\OwnedProperty;
use App\ProfilePicture;
use App\Property;
use App\SoftSkill;
use App\StolenCar;
use App\User;
use App\UserLicenses;
use App\Whitelist;
use App\WhitelistApplications;
use Illuminate\Contracts\View\Factory as Factory;
use Illuminate\Http\RedirectResponse as RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View as View;

class HomeController extends Controller
{

    /**
     * @var string
     */
    protected $steamId;

    /**
     * @var User
     */
    protected $fiveMUser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard
     *
     * @return Factory|View
     * @throws \Exception
     */
    public function index()
    {

        $whitelistApplication = Auth::user()->whitelistApplication()->first();
        /*
         * If user whitelist application is not accepted, then  redirect to application status page
         */
        if ($whitelistApplication and !$whitelistApplication->isAccepted) {
            return redirect(route('whitelistApplicationStatus'));
        }

        /*
         * If user have made whitelist application, application is accepted, but user web account is not connected with
         * FiveM server user account
         */
        if (
            $whitelistApplication
            and $whitelistApplication->isAccepted
            and !WebUserHelper::checkWebUserConnectionWithFiveMUser(Auth::user()->id)
        ) {

            if (!User::find($whitelistApplication->user_identifier)) {

                return redirect(route('whitelistApplicationStatus'));
            }

            Auth::user()->fiveMUser()->attach($whitelistApplication->user_identifier);
        }

        if ((!$whitelistApplication and !WebUserHelper::checkWebUserConnectionWithFiveMUser(Auth::user()->id) or !Auth::user()->fiveMUser()->first())) {
            return redirect(route('whitelistApplicationStatus'));
        }

        // Continue with normal home page actions

        $ownedProperties = [];
        foreach (OwnedProperty::where('owner', $this->getSteamId())->get() as $ownedProperty) {
            $property = new \stdClass();
            $property->name = $ownedProperty->name;
            $property->isRented = $ownedProperty->rented;
            $property->label = Property::where('name', $ownedProperty->name)->first()->label;
            $ownedProperties[] = $property;
        }

        foreach (OwnedMotel::where('owner', $this->getSteamId())->get() as $ownedMotel) {
            $property = new \stdClass();
            $property->name = $ownedMotel->name;
            $property->isRented = true;
            $property->label = Motel::where('name', $ownedMotel->name)->first()->label;
            $ownedProperties[] = $property;
        }

        $bills = Bill::where('identifier', $this->getSteamId())->get();
        $billsExtraData =collect(
            [
                'billCount' => $bills->count(),
                'billSum' => ($bills->count() > 0) ? $bills->sum->amount : 0,
                'billPolice' => $bills->where('target', 'society_police'),
                'billAmbulance' => $bills->where('target', 'society_ambulance'),
                'billTaxi' => $bills->where('target', 'society_taxi'),
                'billMechanic' => $bills->where('target', 'society_mechanic'),
                'billCarDealer' => $bills->where('target', 'society_cardealer'),
                'billOther' => $bills->whereNotIn('target', ['society_mechanic', 'society_cardealer', 'society_taxi', 'society_ambulance', 'society_police'])
            ]
        );

        $userData = Auth::user()->fiveMUser()->first();
        $companyInfo = [
            'money' => 0,
            'name' => ''
        ];

        if( isset($userData->job_grade) and $userData->job_grade >= 99 ) {
            $companyAddonAccountData = AddonAccountData::where('account_name', 'society_' . $userData->job)->first();
            $companyInfo['money'] = ($companyAddonAccountData and isset($companyAddonAccountData->money)) ? $companyAddonAccountData->money : 0;
            $companyInfo['name'] = Job::where( 'name', $userData->job )->first()->label;
        }

        return view(
            'user.home',
            [
                'steamId' => $this->getSteamId(),
                'userData' => $userData,
                'bills' => $bills,
                'billsExtraData' => $billsExtraData,
                'licenses' => UserLicenses::where('owner', $this->getSteamId())->get(),
                'characters' => Character::where('identifier', $this->getSteamId())->get(),
                'ownedProperties' => $ownedProperties,
                'companyInfo' => $companyInfo,
                'skills' => SoftSkill::where('identifier', $this->getSteamId())->get(),
                'profilePicture' => (Auth::user()->profilePicture()->orderBy('id', 'DESC')->first()) ? Auth::user()->profilePicture()->orderBy('id', 'DESC')->first()->path : null,
            ]
        );
    }

    /**
     * @return Factory|View
     * @throws \Exception
     */
    public function cars()
    {

        return view(
            'user.cars',
            [
                'steamId' => $this->getSteamId(),
                'userData' => Auth::user()->fiveMUser()->first(),
                'cars' => OwnedCar::where('owner', $this->getSteamId())->get(),
                'carSellRequests' => CarSellRequest::where('sellerId', $this->getSteamId())->whereNull('confirmedBy')->get(),
            ]
        );
    }

    /**
     * @return Factory|View
     * @throws \Exception
     */
    public function criminalRecords()
    {

        return view(
            'user.criminalRecords',
            [
                'steamId' => $this->getSteamId(),
                'userData' => $this->getFiveMUser(),
                'criminalRecords' => $this->getFiveMUser()->criminalRecords()->get(),
                'characters' => Character::where('identifier', $this->getSteamId())->get(),
            ]
        );
    }

    public function medicalRecords()
    {


        return view(
            'user.medicalRecords',
            [
                'steamId' => $this->getSteamId(),
                'characters' => Character::where('identifier', $this->getSteamId())->get(),
                'medicalHistory' => Auth::user()->getFivemUserData()->medicalHistory()->get(),
            ]
        );
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function sellCar(Request $request)
    {

        try {
            $carSellRequest = new CarSellRequest();

            if (CarSellRequest::where('plate', $request->carPlate)->whereNull('confirmedBy')->first()) {
                return redirect()->back()->with('carSellRequestExist', true);
            }
            $carSellRequest->sellerId = $request->sellerId;
            $carSellRequest->buyerId = $request->buyerId;
            $carSellRequest->plate = $request->carPlate;
            $carSellRequest->save();

            return redirect()->back()->with('carSellRequestStatus', true);

        } catch (\Exception $exception) {
            Log::error('Error in car sale: ' . $exception->getMessage());

            return redirect()->back()->with('carSellRequestStatus', false);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function cancelCarSell(Request $request)
    {

        CarSellRequest::destroy($request->sellRequestId);

        return redirect()->back()->with('carSellRequestDeleted', true);
    }

    /**
     * @return Factory|View
     */
    public function whitelistApplicationStatus()
    {

        return view(
            'user.applicationStatus',
            [
                'whitelistApplication' => Auth::user()->whitelistApplication()->first()
            ]
        );
    }

    /**
     * @return Factory|View
     */
    public function whitelistApplicationUpdateView()
    {

        return view('user.whitelistApplication', [
            'whitelistData' => Auth::user()->whitelistApplication()->first()
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function updateWhitelistApplication(Request $request)
    {

        request()->validate([
            'characterName' => 'required|string|min:2|max:50',

            'steamName' => 'required|string|min:2|max:20',

            'hex' => 'required|string|min:2|max:50'
        ], [
            'characterName.required' => __('wlValidationErrors.character_required'),

            'characterName.min' => __('wlValidationErrors.character_min'),

            'characterName.max' => __('wlValidationErrors.character_max'),

            'steamName.required' => __('wlValidationErrors.steam_required'),

            'steamName.min' => __('wlValidationErrors.steam_min'),

            'steamName.max' => __('wlValidationErrors.steam_max')

        ]);

        $steamId = 'steam:' . strtolower($request->hex);

        if (Whitelist::where('identifier', $steamId)->orWhere('identifier', 'ban:' . $request->hex)->first()) {
            return redirect()->back()->withInput($request->all())->withErrors(['hex' => __('wlValidationErrors.already_whitelisted')]);
        }

        if (FivemUserHelper::searchUsers($request->characterName)->count() > 0) {
            return redirect()->back()->withInput($request->all())->withErrors(['characterName' => __('wlValidationErrors.character_name_exists')]);
        }

        // List of extra questions names
        $extraQuestionsList = [
            'whenYouCanUseWeapons',
            'newLifeRule',
            'helmetMaskRules',
            'robbingOtherCitizen',
            'carTax',
            'vdmRdm',
            'oocIc',
            'radioUsage',
        ];

        $extraQuestionsAndAnswers = [];
        foreach ($request->all() as $question => $answer) {
            if (in_array($question, $extraQuestionsList)) {
                $extraQuestionsAndAnswers[$question] = $answer;
            }
        }

        $application = WhitelistApplications::where('web_user_id', Auth::user()->id)->first();
        $application->user_identifier = $steamId;
        $application->steamName = $request->steamName;
        $application->characterName = $request->characterName;
        $application->isAccepted = false;
        $application->isRejected = false;
        $application->extra_questions = json_encode($extraQuestionsAndAnswers);
        $application->save();

        return redirect(route('whitelistApplicationStatus'));
    }

    /**
     * @return Factory|View
     * @throws \Exception
     */
    public function showJobApplications()
    {

        return view(
            'user.myJobApplications',
            [
                'applications' => Auth::user()->jobApplication()->orderBy('id', 'DESC')->get()
            ]
        );
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function unDeclareCarAsStolen(Request $request)
    {

        $stolenCar = StolenCar::where('plate', $request->plate)->first();

        if(!$stolenCar) {
            return redirect()->back()->with('error', 'Car with this plate is not in stolen cars list');
        }

        $stolenCar->is_stolen = false;
        $stolenCar->save();

        return redirect()->back()->with('status', 'Car is not stolen any more');
    }

    public function uploadProfilePicture(Request $request)
    {

        try {

            request()->validate([

                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048',

            ]);

            $uploadedImage = Storage::disk('local')->put('public/profile/pictures', $request->image);
            $profilePicture = new ProfilePicture();
            $profilePicture->path = $uploadedImage;
            $profilePicture->web_user_id =Auth::id();
            $profilePicture->save();

            return redirect()->back()->with('status',  __('messages.picture_upload_success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error',  __('messages.picture_upload_failed'));
        }
    }

    /**
     * @return User
     */
    private function getFiveMUser()
    {

        if(!$this->fiveMUser) {
            $this->fiveMUser = Auth::user()->getFiveMUserData();
        }

        return $this->fiveMUser;
    }

    /**
     * @return string
     */
    private function getSteamId()
    {

        if (!$this->steamId) {
            $this->steamId = Auth::user()->fiveMUser()->first()->identifier;
        }

        return $this->steamId;
    }
}
