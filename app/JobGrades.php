<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobGrades extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_grades';
}
