<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWantedCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wanted_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plate');
            $table->boolean('is_wanted');
            $table->string('reason');
            $table->string('image')->nullable();
            $table->string('police_id');
            $table->string('officer_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wanted_cars');
    }
}
