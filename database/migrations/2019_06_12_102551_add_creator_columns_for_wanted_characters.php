<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatorColumnsForWantedCharacters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('wanted_characters', function (Blueprint $table) {
            $table->string('creator_id')->nullable();
            $table->string('creator_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('wanted_characters', function (Blueprint $table) {
            $table->dropColumn(['creator_id', 'creator_name']);
        });
    }
}
