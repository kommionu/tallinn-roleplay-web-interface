<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterKillLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_kill_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('adminName');
            $table->string('adminIdentifier');
            $table->string('userName');
            $table->string('userIdentifier');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_kill_logs');
    }
}
