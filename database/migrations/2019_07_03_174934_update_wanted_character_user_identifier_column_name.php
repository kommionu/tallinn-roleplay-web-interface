<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWantedCharacterUserIdentifierColumnName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('wanted_characters', function (Blueprint $table) {
            $table->renameColumn('criminal_steam_id', 'user_identifier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('wanted_characters', function (Blueprint $table) {
            $table->renameColumn('user_identifier', 'criminal_steam_id');
        });
    }
}
