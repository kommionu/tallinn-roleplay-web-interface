<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'WebUser who order has all privilege';
        $role_admin->save();

        $role_player = new Role();
        $role_player->name = 'player';
        $role_player->description = 'WebUser who plays in Our server';
        $role_player->save();
    }
}
